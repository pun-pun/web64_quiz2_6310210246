const bcrypt = require('bcrypt')
const SALT_ROUNDS = 10

const mysql = require('mysql')

const jwt = require('jsonwebtoken')
const dotenv = require('dotenv')
dotenv.config()
const TOKEN_SECRET = process.env.TOKEN_SECRET

const connection = mysql.createConnection({
    host : 'localhost',
    user: 'root',
    password: 'root',
    database: 'DormitorySystem'

})

connection.connect()

const express = require('express')
const req = require('express/lib/request')
const app = express()
const port = 4000

app.post('/login', (req, res) => {
    let username = req.query.username
    let user_password = req.query.password
    let query = `SELECT * FROM Residents WHERE Username='${username}'`

    connection.query( query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({
                        "status" : "400", 
                        "message" : "Error querying from running db"
                    })
        }else {
            let db_password = rows[0].Password
            bcrypt.compare(user_password, db_password, (err, result) => {
                if (result) {
                    let payload = {
                        "username" : rows[0].Username, 
                        "res_id" : rows[0].ResId, 
                        "IsAdmin" : rows[0].IsAdmin
                    }
                    console.log(payload)
                    let token = jwt.sign(payload, TOKEN_SECRET, { expiresIn : '1d'})
                    res.send(token)
                }else { res.send("Invalid username / password") }
            })
        }
    })
})

/*query = "SELECT * from Residents";
connection.query( query, (err, rows) => {
    if (err) {
        console.log(err);
    
    }else {
        console.log(rows);
    }
})*/

app.listen(port, () => {
    console.log(` Now starting Running System Backend ${port}`)
})

app.post('/register_resident', (req, res) => {
    let res_name = req.query.res_name
    let res_surname = req.query.res_surname
    let res_username = req.query.res_username
    let res_password = req.query.res_password
    
    bcrypt.hash(res_password, SALT_ROUNDS, (err, hash) => {
        let query = `INSERT INTO Residents (ResunnerName, ResSurname, Username, Password, IsAdmin) 
                    VALUES ('${res_name}','${res_surname}',
                    '${res_username}', '${hash}', false)`
        console.log(query)
    
        connection.query( query, (err, rows) => {
            if (err) {
                console.log(err)
                res.json({ "status" : "400",
                            "message" : "Error inserting data into db"
                        })
            }else {
                res.json({ "status" : "200",
                            "message" : "Adding new user successful"
                    })
            }
        });
    })
})

function authenticateToken(req, res, next) {
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]
    if (token == null) return res.sendStatus(401)
    jwt.verify(token, TOKEN_SECRET, (err, user) => {
        if (err) { return res.sendStatus(403) }
        else {
            req.user = user
            next()
        }
    })
}

app.post('/list_resevent', (req, res) => {
    query = "SELECT * from EventRes"
    connection.query( query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({ "status" : "400",
                        "message" : "Error querying from running db"
                    })
        }else {
            res.json(rows)
        }
    });
})

app.post("/add_resid", (req, res) => {
    let res_name = req.query.res_name
    let res_surname = req.query.res_surname

    let query = `INSERT INTO Residents (ResName, ResSurname) 
                VALUES ('${res_name}','${res_surname}')`
    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({ "status" : "400",
                        "message" : "Error inserting data into db"
                    })
        }else {
            res.json({ "status" : "200",
                        "message" : "Adding event successful"
                    })
        }
    });

})

app.post("/update_resevent", (req, res) => {

    let resevent_id = req.query.resevent_id
    let room_id = req.query.room_id
    let res_id = req.query.res_id

    let query = `UPDATE EventRes SET 
                RoomID='${room_id}',
                ResID='${res_id}'
                Where EventResID=${resevent_id}`
    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({ "status" : "400",
                        "message" : "Error updateing record"
                    })
        }else {
            res.json({ "status" : "200",
                        "message" : "Updating event succesful"
                    })
        }
    });

})

app.post("/delete_resevent", (req, res) => {

    let resevent_id = req.query.resevent_id

    let query = `DELETE FROM EventRes WHERE EventResID=${resevent_id}`
    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({ "status" : "400",
                        "message" : "Error deleting record"
                    })
        }else {
            res.json({ "status" : "200",
                        "message" : "Deleting record succes"
                    })
        }
    });

})

app.post('/list_reg_by_resid', authenticateToken, (req, res) => {

    let res_name = req.user.res_name

    if (!req.user.IsAdmin) { res.send("Unauthorized") }
    else {
        let query = `
        SELECT Residents.ResID, EventRes.EventResID 
                
            FROM Residents, ResEvent
                WHERE ( Residents.ResID = Residents.ResID ) AND 
                    ( EventRes.EventID = EventRes.EventResID ) AND 
                    ( Residents.ResID = ${res_name} );`

        connection.query( query, (err, rows) => {
            if (err) {
                console.log(err)
                res.json({ "status" : "400",
                            "message" : "Error querying from running db"
                        })
            }else {
                res.json(rows)
            }
        });
    }
    
})

app.post('/register_resevent', authenticateToken, (req, res) => {
    
    let res_id = req.user.res_id
    let resevent_id = req.query.resevent_id

    if (!req.user.IsAdmin) { res.send("Forbidden")}
    else {
    let query = `INSERT INTO Residents
                    (ResID, EventResID) 
                    VALUES ('${res_id}',
                            '${resevent_id}')`
    console.log(query)

    
    connection.query( query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({ "status" : "400",
                        "message" : "Error inserting data into db"
                    })
        }else {
            res.json({ "status" : "200",
                        "message" : "Adding event successful"
                    })
            }
        });
    }
})